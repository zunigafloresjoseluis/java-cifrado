/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeros;

/**
 *
 * @author Administrador
 */
public class Numero {
    
    public boolean esNumeroDigitoAcendente(int numero){
        return auxiliarNumeroAcendente(numero/10, numero%10,true);
    }
    
    private boolean auxiliarNumeroAcendente(int numero,int valorPrevio,boolean exito){
        if(numero!=0 && exito){
            exito = auxiliarNumeroAcendente(numero/10, numero%10, valorPrevio > numero%10);
        }
        return exito;
    }
    
    
    public boolean esCantidadNumero(int numero){
        return auxiliarCantidadNumero(numero%10,numero/10,numero,true);
    }
    
    public boolean auxiliarCantidadNumero(int numero,int cifra,int cifrareal){
      if(numero!=contarNumero(numero, cifrareal, 0,true))
         return false;
      if(cifra!=0){
          auxiliarCantidadNumero(cifra%10, cifra/10,cifrareal);
      }
      return true;  
    }
    public int contarNumero(int numero,int cifra,int contador){
        if(numero==(cifra%10))
            contador++;
        if(cifra==0) 
            return contador;
        return contarNumero(numero, cifra/10, contador);
    }
    
    
    
     public int contarNumero(int numero,int cifra,int contador,boolean exito){
        if(exito){
            return contarNumero(numero,cifra/10,contador+=Boolean.compare(numero==(cifra%10), Boolean.TRUE) + 1,(cifra!=0));
        }
        return contador;
    }
    
    public boolean auxiliarCantidadNumero(int numero,int cifra,int cifrareal,boolean exito){
      if( exito && cifra!=0 )
         exito= auxiliarCantidadNumero(cifra%10, cifra/10,cifrareal,numero==contarNumero(numero, cifrareal, 0,true));
      
      return exito;  
    }
     
     
}
