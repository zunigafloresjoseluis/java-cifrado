/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones.Cifra;


public class ExcepcionCifrado extends Exception {
    public ExcepcionCifrado(){
        super("Error de Cifrado: El mensajeOriginal o mensajeSecuencia son nullos o tiene alfabetos distintos");
    }
}
