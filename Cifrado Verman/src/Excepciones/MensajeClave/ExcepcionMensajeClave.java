/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones.MensajeClave;


public class ExcepcionMensajeClave extends Exception{
    public ExcepcionMensajeClave(){
        super("Error Mensaje Clave: Los caracteres del alfabeto deben coincidir con los caracteres de la cadena ");
    }
}
