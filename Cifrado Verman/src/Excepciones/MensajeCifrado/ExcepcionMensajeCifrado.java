/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones.MensajeCifrado;


public class ExcepcionMensajeCifrado extends Exception{
    public ExcepcionMensajeCifrado(){
        super("Error en el mensaje cifrado: Alfabeto o binarios nullos");
    }
}
