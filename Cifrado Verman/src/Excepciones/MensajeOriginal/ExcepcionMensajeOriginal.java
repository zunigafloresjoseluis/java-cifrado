/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Excepciones.MensajeOriginal;

/**
 *
 * @author Jarvis
 */
public class ExcepcionMensajeOriginal extends Exception {
    public ExcepcionMensajeOriginal(){
        super("Error en el mensaje : Los caracteres del alfabeto deben coincidir con los caracteres de la cadena");
    }
}
