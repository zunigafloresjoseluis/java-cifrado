/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Deficrado;

import Cadena.Cadena;
import Excepciones.Decifrado.ExcepcionDecifradoVernam;
import Excepciones.ExcepcionCadena.ExcepcionCadena;
import Excepciones.MensajeCifrado.ExcepcionMensajeCifrado;
import Excepciones.MensajeDecifrado.ExcepcionMensajeDecifrado;
import Interfaces.Cadena.InterfazCadena;
import Interfaces.Decifrado.Decifrado;
import Interfaces.Mensaje.Mensaje;

import Mensaje.Decifrado.MensajeDecifrado;
import java.util.HashMap;
import java.util.Map;


public class DecifradoVernam implements Decifrado{
    private MensajeDecifrado mensajeDecifrado;
    
    public DecifradoVernam(Mensaje mensajeCifrado,Mensaje mensajeClave) throws ExcepcionDecifradoVernam, ExcepcionCadena, ExcepcionMensajeCifrado, ExcepcionMensajeDecifrado{
        if(mensajeCifrado == null || mensajeClave == null)
            throw new ExcepcionDecifradoVernam();
        mensajeDecifrado=decifrar(mensajeCifrado, mensajeClave);
    }
    
      private MensajeDecifrado decifrar(Mensaje mensajeCifrado,Mensaje mensajeSecuencia) throws ExcepcionCadena, ExcepcionMensajeCifrado, ExcepcionMensajeDecifrado{
        Map<Integer,InterfazCadena> binarios=new HashMap();
        for(Integer key : mensajeCifrado.getBinarios().keySet())
           binarios.put(key, aplicarXORaCaracter(mensajeCifrado.getBinarios().get(key), mensajeSecuencia.getBinarios().get(key),new Cadena("")));
        return new MensajeDecifrado(binarios,mensajeCifrado.getAlfabeto());
    }
    
    private InterfazCadena aplicarXORaCaracter(InterfazCadena cadenaMensajeCifrar, InterfazCadena cadenaMensajeSecuencia,InterfazCadena cadenaDecifrado ) throws ExcepcionCadena{
       for(Integer i = cadenaMensajeCifrar.length()-2;i >=0; i--){
            Byte byteMensajeOriginal=new Byte(cadenaMensajeCifrar.charAt(i)+"");
            Byte byteMensajeSecuencia=new Byte(cadenaMensajeSecuencia.charAt(i)+"");
            cadenaDecifrado=new Cadena((byteMensajeOriginal^byteMensajeSecuencia)+cadenaDecifrado.toString());
        }
        return cadenaDecifrado;
    
    }

    @Override
    public MensajeDecifrado getMensajeCifrado() {
       return mensajeDecifrado;
    }

  
    
   
    
}
