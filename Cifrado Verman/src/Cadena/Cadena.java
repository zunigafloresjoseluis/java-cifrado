/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cadena;
 
import Excepciones.ExcepcionCadena.ExcepcionCadena;
import java.util.HashMap;
import java.util.Map;
import Interfaces.Cadena.InterfazCadena;

public class Cadena implements InterfazCadena{
     private Map <Integer,Character> cadena;
    
    public Cadena(String cadena) throws ExcepcionCadena{
        if(cadena == null)
            throw new ExcepcionCadena();
        this.cadena = new HashMap<Integer,Character>();
        crearCadena(cadena);
    }
    
    private void crearCadena(String cadena){
        for(Integer i = 0; i < cadena.length(); i++){
            this.cadena.put(i, cadena.charAt(i));
        }
    } 
    
  
    
    @Override
    public Character charAt(Integer indice) {
        return cadena.get(indice);
    }

    @Override
    public Integer length() {
        return cadena.size();
    }
    
    @Override
    public String toString(){
        return auxiliarToString(0);
    }
    
    private String auxiliarToString(Integer indice){
        if(indice<6 && cadena.get(indice)!=null)
            return cadena.get(indice)+auxiliarToString(indice+1);
        return cadena.get(indice)+"";
    }
    
}
