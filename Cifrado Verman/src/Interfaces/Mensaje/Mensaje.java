/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces.Mensaje;

import Interfaces.Alfabeto.Alfabeto;
import Interfaces.Cadena.InterfazCadena;
import java.util.Map;

 
public interface Mensaje {
    public String getMensaje();
    public Alfabeto getAlfabeto();
    public Map<Integer,InterfazCadena> getBinarios();
}
