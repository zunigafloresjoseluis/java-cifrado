/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces.Alfabeto;

import Cadena.Cadena;
import Excepciones.ExcepcionCadena.ExcepcionCadena;


public interface Alfabeto {
    public Character getCaracter(Integer indice);
    public Integer getPosicionCaracter(Character caracter);
    public Integer getTotalCaracteres();
    public Cadena getCaracterBinario(Character caracter)throws ExcepcionCadena;
}
