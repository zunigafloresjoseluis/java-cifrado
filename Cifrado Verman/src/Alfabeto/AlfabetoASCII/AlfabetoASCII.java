/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alfabeto.AlfabetoASCII;

import Cadena.Cadena;
import Excepciones.ExcepcionCadena.ExcepcionCadena;
import Interfaces.Alfabeto.Alfabeto;
import java.util.HashMap;
import java.util.Map;
 
public class AlfabetoASCII implements Alfabeto{
    private Map<Character,Integer> listaCaracteres;
    
    public  AlfabetoASCII(){
        listaCaracteres= new HashMap <Character,Integer>();
        listaCaracteres.put('A',1);
        listaCaracteres.put('B',2);
        listaCaracteres.put('C',3);
        listaCaracteres.put('D',4);
        listaCaracteres.put('E',5);
        listaCaracteres.put('F',6);
        listaCaracteres.put('G',7);
        listaCaracteres.put('H',8);
        listaCaracteres.put('I',9);
        listaCaracteres.put('J',10);
        listaCaracteres.put('K',11);
        listaCaracteres.put('L',12);
        listaCaracteres.put('M',13);
        listaCaracteres.put('N',14);
        listaCaracteres.put('O',15);
        listaCaracteres.put('P',16);
        listaCaracteres.put('Q',17);
        listaCaracteres.put('R',18);
        listaCaracteres.put('S',19);
        listaCaracteres.put('T',20);
        listaCaracteres.put('U',21);
        listaCaracteres.put('V',22);
        listaCaracteres.put('W',23);
        listaCaracteres.put('X',24);
        listaCaracteres.put('Y',25);
        listaCaracteres.put('Z',26);
        listaCaracteres.put('a',27);
        listaCaracteres.put('b',28);
        listaCaracteres.put('c',29);
        listaCaracteres.put('d',30);
        listaCaracteres.put('e',31);
        listaCaracteres.put('f',32);
        listaCaracteres.put('g',33);
        listaCaracteres.put('h',34);
        listaCaracteres.put('i',35);
        listaCaracteres.put('j',36);
        listaCaracteres.put('k',37);
        listaCaracteres.put('l',38);
        listaCaracteres.put('m',39);
        listaCaracteres.put('n',40);
        listaCaracteres.put('o',41);
        listaCaracteres.put('p',42);
        listaCaracteres.put('q',43);
        listaCaracteres.put('r',44);
        listaCaracteres.put('s',45);
        listaCaracteres.put('t',46);
        listaCaracteres.put('u',47);
        listaCaracteres.put('v',48);
        listaCaracteres.put('w',49);
        listaCaracteres.put('x',50);
        listaCaracteres.put('y',51);
        listaCaracteres.put('z',52);
       
    }

   

   @Override
    public Character getCaracter(Integer indice){
        for(Character caracter : listaCaracteres.keySet()){  
            if(listaCaracteres.get(caracter).equals(indice))
                return caracter;
        }
        return '*';
    }
    
    @Override
    public Integer getPosicionCaracter(Character caracter){
        return listaCaracteres.get(caracter);
    }
    
    @Override
    public Integer getTotalCaracteres(){
        return listaCaracteres.size();
    }

    @Override
    public Cadena getCaracterBinario(Character caracter) throws ExcepcionCadena {
        Character caracterTemp=getCaracter(getPosicionCaracter(caracter));
        Integer numeroASCII=new Integer(caracterTemp);
        return new Cadena(Integer.toBinaryString(numeroASCII));
    }
     
     
    
}
