/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba;

import Alfabeto.AlfabetoASCII.AlfabetoASCII;

import Cifrado.CifradoVerman;
import Deficrado.DecifradoVernam;
import Excepciones.Cifra.ExcepcionCifrado;
import Excepciones.Decifrado.ExcepcionDecifradoVernam;
import Excepciones.ExcepcionCadena.ExcepcionCadena;
import Excepciones.MensajeCifrado.ExcepcionMensajeCifrado;
import Excepciones.MensajeClave.ExcepcionMensajeClave;
import Excepciones.MensajeDecifrado.ExcepcionMensajeDecifrado;
import Excepciones.MensajeOriginal.ExcepcionMensajeOriginal;
import Interfaces.Alfabeto.Alfabeto;
import Interfaces.Cifrado.Cifrado;
import Interfaces.Decifrado.Decifrado;
import Interfaces.Mensaje.Mensaje;
import Mensaje.MensajeClaveSecuencia.MensajeClaveSecuencia;
import Mensaje.Original.MensajeOriginal;



public class Prueba {

    public static void main(String[] args) throws  ExcepcionMensajeCifrado, ExcepcionCadena, ExcepcionMensajeOriginal, ExcepcionMensajeClave, ExcepcionCifrado, ExcepcionDecifradoVernam, ExcepcionMensajeDecifrado {
      
        Alfabeto alfabetoASCII = new AlfabetoASCII();
        Mensaje mensajeOriginal=new MensajeOriginal("holas",alfabetoASCII);
        System.out.println("Mensaje  :"+mensajeOriginal.getMensaje());
        Mensaje mensajeClave=new MensajeClaveSecuencia("purso",alfabetoASCII,mensajeOriginal);
        System.out.print("Mensaje bits           ");
        mensajeOriginal.getBinarios().keySet().forEach((key) -> {
            System.out.print(mensajeOriginal.getBinarios().get(key)+" ");
        });
        
        System.out.println();
        System.out.print("Mensaje Secuencia bits ");
        mensajeClave.getBinarios().keySet().forEach((key) -> {
            System.out.print(mensajeClave.getBinarios().get(key)+" ");
        });
        
        System.out.println();
        
        Cifrado cifradoVerman=new CifradoVerman(mensajeOriginal, mensajeClave);
        Mensaje mensajeCifrado= cifradoVerman.getMensajeCifrado();
        System.out.print("Mensaje Cifrado bits   ");
        mensajeCifrado.getBinarios().keySet().forEach((key) -> {
            System.out.print(mensajeCifrado.getBinarios().get(key)+" ");
        });
        System.out.println();
        Decifrado decifradoVerman=new DecifradoVernam(mensajeCifrado,mensajeClave);
        Mensaje mensajeDecifrado=decifradoVerman.getMensajeCifrado();
        System.out.print("Mensaje Decifrado bits ");
        mensajeDecifrado.getBinarios().keySet().forEach((key) -> {
            System.out.print(mensajeDecifrado.getBinarios().get(key)+" ");
        });
        
        System.out.println("\nMensaje decifrado "+mensajeDecifrado.getMensaje());
            
        
    }
    
   
    
    
}
