/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cifrado;

import Cadena.Cadena;
import Excepciones.Cifra.ExcepcionCifrado;
import Excepciones.ExcepcionCadena.ExcepcionCadena;
import Excepciones.MensajeCifrado.ExcepcionMensajeCifrado;
import Interfaces.Cadena.InterfazCadena;
import Interfaces.Cifrado.Cifrado;
import Interfaces.Mensaje.Mensaje;
import Mensaje.Cifrado.MensajeCifrado;
import java.util.HashMap;
import java.util.Map;



public class CifradoVerman implements Cifrado {
    private MensajeCifrado mensajeCifrado;
    
    public CifradoVerman(Mensaje mensajeOriginal,Mensaje mensajeSecuencia) throws ExcepcionCifrado, ExcepcionCadena, ExcepcionMensajeCifrado{
        if(mensajeOriginal == null || mensajeSecuencia == null || mensajeOriginal.getAlfabeto()!=mensajeSecuencia.getAlfabeto())
            throw new ExcepcionCifrado();
        mensajeCifrado=cifrar(mensajeOriginal,mensajeSecuencia);
        
    }
    
    private MensajeCifrado cifrar(Mensaje mensajeOriginal,Mensaje mensajeSecuencia) throws ExcepcionCadena, ExcepcionMensajeCifrado{
        Map<Integer,InterfazCadena> binarios=new HashMap();
        for(Integer key : mensajeOriginal.getBinarios().keySet())
           binarios.put(key, aplicarXORaCaracter(mensajeOriginal.getBinarios().get(key), mensajeSecuencia.getBinarios().get(key),new Cadena("")));
        return new MensajeCifrado(binarios,mensajeOriginal.getAlfabeto());
    }
    
    private InterfazCadena aplicarXORaCaracter(InterfazCadena cadenaMensajeOriginal, InterfazCadena cadenaMensajeSecuencia,InterfazCadena cadenaCifrado ) throws ExcepcionCadena{
        for(Integer i = cadenaMensajeOriginal.length()-1;i >=0; i--){
            Byte byteMensajeOriginal=new Byte(cadenaMensajeOriginal.charAt(i)+"");
            Byte byteMensajeSecuencia=new Byte(cadenaMensajeSecuencia.charAt(i)+"");
            cadenaCifrado=new Cadena((byteMensajeOriginal^byteMensajeSecuencia)+cadenaCifrado.toString());
        }
        return cadenaCifrado;
    
    }
    
    
    @Override
    public MensajeCifrado getMensajeCifrado() {
        return mensajeCifrado;
    }
    
    
    
    
}
