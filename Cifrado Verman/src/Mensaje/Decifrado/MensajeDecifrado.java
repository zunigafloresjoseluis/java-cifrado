/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.Decifrado;

import Excepciones.MensajeDecifrado.ExcepcionMensajeDecifrado;
import Interfaces.Alfabeto.Alfabeto;
import Interfaces.Cadena.InterfazCadena;
import Interfaces.Mensaje.Mensaje;
import java.util.Map;


public class MensajeDecifrado implements Mensaje{
    private String mensajeDecifrado;
    private Alfabeto alfabeto;
    private Map<Integer,InterfazCadena> binarios; 
    public MensajeDecifrado(Map<Integer,InterfazCadena>binarios,Alfabeto alfabeto) throws ExcepcionMensajeDecifrado{
        if(alfabeto == null || binarios == null || binarios.isEmpty() )
            throw  new ExcepcionMensajeDecifrado();
        this.alfabeto=alfabeto;
        this.binarios=binarios;
        mensajeDecifrado="";
        convertirBinarioaMensaje();
    }
    
    private Integer deBinarioADecimal( Integer exponente,Integer decimal,Integer numero){
        if(numero!=0){
             Integer digito = numero % 10;
               decimal = decimal + digito * (int) Math.pow(2, exponente);
               return deBinarioADecimal(exponente+1, decimal, numero/10);
        }
        return decimal;
    }
    
    private void convertirBinarioaMensaje(){
        for(Integer key : binarios.keySet()){  
            Integer valorAscii=deBinarioADecimal(0, 0,Integer.parseInt(binarios.get(key).toString()));
            mensajeDecifrado+=buscarLetra(valorAscii,"");
        }
    }
    
    private CharSequence buscarLetra(Integer valorAscii,CharSequence a){
        for(Integer i=0;i<alfabeto.getTotalCaracteres();i++){
            Integer valorAlfabetoDecimal=new Integer(alfabeto.getCaracter(i));
            if(valorAlfabetoDecimal.intValue() == valorAscii.intValue())
               a=a.toString()+alfabeto.getCaracter(i);
            
        }
        return a;
    }
    
    
    @Override
    public String getMensaje() {
        return mensajeDecifrado;
    }

    @Override
    public Alfabeto getAlfabeto() {
       return alfabeto;
    }

    @Override
    public Map<Integer, InterfazCadena> getBinarios() {
        return binarios;
    }
    
 
    
}
