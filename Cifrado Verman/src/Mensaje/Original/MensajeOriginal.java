/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.Original;

import Excepciones.ExcepcionCadena.ExcepcionCadena;
import Excepciones.MensajeOriginal.ExcepcionMensajeOriginal;
import Interfaces.Alfabeto.Alfabeto;
import Interfaces.Cadena.InterfazCadena;
import Interfaces.Mensaje.Mensaje;
import java.util.HashMap;
import java.util.Map;


public class MensajeOriginal implements Mensaje{
    private String mensaje;
    private Alfabeto alfabeto;
    private Map<Integer,InterfazCadena> binarios;
            
            
    public MensajeOriginal(String mensaje,Alfabeto alfabeto) throws ExcepcionMensajeOriginal, ExcepcionCadena{
      
        this.mensaje=mensaje;
        this.alfabeto=alfabeto;
        validacionMensaje(this);
        hacerBinarioPorCaracter();
        
    }
    
    private void validacionMensaje(Mensaje mensaje) throws ExcepcionMensajeOriginal{
        if(mensaje.getMensaje().isEmpty() || mensaje.getMensaje()==null || alfabeto == null)
            throw new ExcepcionMensajeOriginal();
        validarCaracteresEnAlfabeto(mensaje);
    }
    private void validarCaracteresEnAlfabeto(Mensaje mensaje) throws ExcepcionMensajeOriginal{
        for(Integer i=0;i < mensaje.getMensaje().length();i++)
            if(alfabeto.getPosicionCaracter(mensaje.getMensaje().charAt(i))==null)
                throw new ExcepcionMensajeOriginal();
    }
    
    private void hacerBinarioPorCaracter() throws ExcepcionCadena{
        this.binarios = new HashMap<>();
        for(Integer i=0;i<mensaje.length();i++){
             binarios.put(i, alfabeto.getCaracterBinario(mensaje.charAt(i)));
        }
        
    }
    
    
    @Override
    public String getMensaje() {
        return mensaje;
    }

    @Override
    public Alfabeto getAlfabeto() {
        return alfabeto;
    }

    @Override
    public Map<Integer,InterfazCadena> getBinarios() {
        return binarios;
    }
    
}
