/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.Cifrado;

import Excepciones.MensajeCifrado.ExcepcionMensajeCifrado;
import Interfaces.Alfabeto.Alfabeto;
import Interfaces.Cadena.InterfazCadena;
import Interfaces.Mensaje.Mensaje;
import java.util.Map;


public class MensajeCifrado implements Mensaje {
    private String mensajeCifrado;
    private Alfabeto alfabeto;
    private Map<Integer,InterfazCadena> binarios; 
    
    public MensajeCifrado(Map<Integer,InterfazCadena> binarios, Alfabeto alfabeto) throws ExcepcionMensajeCifrado{
        if(binarios==null || alfabeto==null)
            throw new ExcepcionMensajeCifrado();
        this.alfabeto=alfabeto;
        this.binarios=binarios;
        
    }   
    
    
    
    @Override
    public String getMensaje() {
        return mensajeCifrado;
    }

    @Override
    public Alfabeto getAlfabeto() {
       return alfabeto;
    }

    @Override
    public Map<Integer, InterfazCadena> getBinarios() {
        return binarios;
    }
    
}
