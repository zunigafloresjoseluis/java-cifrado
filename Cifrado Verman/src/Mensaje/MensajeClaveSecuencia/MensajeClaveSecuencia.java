/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.MensajeClaveSecuencia;

import Cadena.Cadena;
import Excepciones.ExcepcionCadena.ExcepcionCadena;
import Excepciones.MensajeClave.ExcepcionMensajeClave;
import Interfaces.Alfabeto.Alfabeto;
import Interfaces.Cadena.InterfazCadena;
import Interfaces.Mensaje.Mensaje;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
public class MensajeClaveSecuencia implements Mensaje{
    private String mensajeClave;
    private Alfabeto alfabeto;
    private Map<Integer,InterfazCadena> binarios;
    public MensajeClaveSecuencia(String mensaje,Alfabeto alfabeto,Mensaje mensajeOriginal) throws ExcepcionMensajeClave, ExcepcionCadena{
        this.mensajeClave=mensaje;
        this.alfabeto=alfabeto;
        validacionMensaje(this,mensajeOriginal);
        hacerBinarioPorCaracter();
    
    }
    
    
    public MensajeClaveSecuencia(Alfabeto alfabeto,Mensaje mensajeOriginal) throws ExcepcionMensajeClave, ExcepcionCadena{
        if(alfabeto==null || mensajeOriginal==null )
            throw new ExcepcionMensajeClave();
        this.alfabeto=alfabeto;
        generarSecuenciaCaracteres(mensajeOriginal);
        hacerBinarioPorCaracter();
    }
    
    private void generarSecuenciaCaracteres(Mensaje mensajeOrignal) throws ExcepcionCadena{
         mensajeClave="";
        for(Integer i=0;i<mensajeOrignal.getMensaje().length();i++){
            
            Integer indiceRandomAlfabeto=new Random().nextInt(alfabeto.getTotalCaracteres())+1;
            mensajeClave+=alfabeto.getCaracter(indiceRandomAlfabeto);
            
        }
        
    }
    
    private void validacionMensaje(Mensaje mensaje,Mensaje mensajeOriginal) throws ExcepcionMensajeClave{
        if(mensaje.getMensaje().isEmpty() || mensaje.getMensaje()==null || alfabeto == null)
            throw new ExcepcionMensajeClave();
         if(mensajeOriginal.getMensaje().length()!= mensaje.getMensaje().length())
             throw new ExcepcionMensajeClave();
        validarCaracteresEnAlfabeto(mensaje);
    }
    private void validarCaracteresEnAlfabeto(Mensaje mensaje) throws ExcepcionMensajeClave{
        for(Integer i=0;i < mensaje.getMensaje().length();i++)
            if(alfabeto.getPosicionCaracter(mensaje.getMensaje().charAt(i))==null)
                throw new ExcepcionMensajeClave();
    }
    
    private void hacerBinarioPorCaracter() throws ExcepcionCadena{
        this.binarios = new HashMap<>();
        for(Integer i=0;i<mensajeClave.length();i++){
             binarios.put(i, alfabeto.getCaracterBinario(mensajeClave.charAt(i)));
        }
        
    }
    
  
    @Override
    public String getMensaje() {
        return mensajeClave;
    }

    @Override
    public Alfabeto getAlfabeto() {
        return alfabeto;
    }

    @Override
    public Map<Integer,InterfazCadena> getBinarios() {
        return binarios;
    }
    
}
