/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cifrado;


import Excepciones.Cifrado.ExcepcionCifradoPorGrupos;
import Excepciones.Periodo.ExcepcionPeriodo;
import Interfaces.Mensaje.Mensaje;
import Mensaje.Cifrado.MensajeCifrado;
import Periodo.Periodo;
import Interfaces.Permutacion.PermutacionInterfaz;
import Interfaces.Cifrado.Cifrador;

public class CifradorPorGrupos implements Cifrador{
    private MensajeCifrado mensajeCifrado;
    private Periodo periodo;
    private Integer contadorPeriodo=0;
    private Integer contadorPermutacion=0;
    private PermutacionInterfaz permutacion;
   
    public CifradorPorGrupos(Mensaje mensaje,PermutacionInterfaz permutacion) throws ExcepcionCifradoPorGrupos, ExcepcionPeriodo{
        if(mensaje == null || permutacion == null)
            throw new ExcepcionCifradoPorGrupos();
        hacerPeriodo(permutacion);
        this.permutacion=permutacion;
        cifrar(mensaje);
    }
    
    private void cifrar(Mensaje mensaje){
        for(contadorPeriodo = 0; contadorPeriodo <= mensaje.getMensaje().length()/this.periodo.getPeriodo(); contadorPeriodo++){
           cifrarGrupo(mensaje);
        }
    }
    
    private void cifrarGrupo(Mensaje mensaje){
        for(contadorPermutacion = 0; contadorPermutacion<this.periodo.getPeriodo(); contadorPermutacion++){
            Integer posicionNuevoCaracter=(contadorPeriodo * periodo.getPeriodo())+Integer.parseInt(permutacion.getPermutacion().toString().charAt(contadorPermutacion)+"")-1;
            if(mensajeCifrado == null ||  posicionNuevoCaracter < mensaje.getMensaje().length())
                mensajeCifrado = new MensajeCifrado(mensaje.getMensaje().charAt(posicionNuevoCaracter)+"", mensajeCifrado);
 
        }
    }
     
    
    
    private void hacerPeriodo(PermutacionInterfaz permutacion) throws ExcepcionPeriodo{
        for(contadorPermutacion = permutacion.getPermutacion(); contadorPermutacion != 0; contadorPermutacion = contadorPermutacion/10){
            contadorPeriodo++;
        }
         this.periodo = new Periodo(contadorPeriodo);
        
    }
    
    @Override
    public MensajeCifrado getMensajeCifrado() {
        return mensajeCifrado;
    }
    
}
