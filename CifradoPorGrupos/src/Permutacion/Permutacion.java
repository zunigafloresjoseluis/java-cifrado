/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Permutacion;

import Excepciones.Cifrado.ExcepcionCifradoPorGrupos;
import Excepciones.Permutacion.ExcepcionPermutacion;
import Interfaces.Permutacion.PermutacionInterfaz;

 
public class Permutacion implements PermutacionInterfaz{
    
    private Integer numeroPermutacion;
    private Integer periodo=0;
    private Integer contador;
    
    public Permutacion(Integer permutacion) throws  ExcepcionPermutacion{
        this.numeroPermutacion = permutacion;
        hacerNumeroPermutacion(permutacion);
        verificarNumeroPermutacion(permutacion);
      
    }
    
    private void hacerNumeroPermutacion(Integer permutacion){
        for(Integer tmpNumeroPermutacion = permutacion ; tmpNumeroPermutacion != 0 ; tmpNumeroPermutacion = tmpNumeroPermutacion/10){
            periodo++;
        } 
    
    }
    
    private void verificarNumeroPermutacion(Integer permutacion) throws ExcepcionPermutacion{
        for(contador = 1 ;contador <= periodo;  contador++){
            Integer tmpNumeroPermutacion = permutacion ;
            verificarDigitoPermutacion(tmpNumeroPermutacion);
                
        }
    
    }
    private void verificarDigitoPermutacion(Integer tmpNumeroPermutacion) throws ExcepcionPermutacion{
         while(contador != tmpNumeroPermutacion%10 && tmpNumeroPermutacion != 0)
            tmpNumeroPermutacion = tmpNumeroPermutacion/10;
            if(tmpNumeroPermutacion == 0)
                throw new ExcepcionPermutacion();
    }
    
    
    @Override
    public Integer getPermutacion() {
        return numeroPermutacion;
    }
}
