/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Descifrado;

import Excepciones.Cifrado.ExcepcionCifradoPorGrupos;
import Excepciones.Periodo.ExcepcionPeriodo;
import Interfaces.Mensaje.Mensaje;
import Mensaje.Decifrado.MensajeDecifrado;
import Periodo.Periodo;
import Interfaces.Permutacion.PermutacionInterfaz;
import Interfaces.Decifrador.Decifrador;
import Interfaces.Periodo.PeriodoInterfaz;

public class DecifradorPorGrupos implements Decifrador{
    private MensajeDecifrado mensajeDecifrado;
    private PeriodoInterfaz periodo;
    private Integer contadorPeriodo=0;
    private Integer contadorPermutacion=0;
    private Integer posicionNuevoCaracter;
    private Integer permutaciontemp;
    private Double lengthmensaje;
    private Double periodoValor;
    public DecifradorPorGrupos(Mensaje mensajeCifrado,PermutacionInterfaz permutacion) throws ExcepcionCifradoPorGrupos, ExcepcionPeriodo{
        if(mensajeCifrado == null || permutacion == null)
            throw new ExcepcionCifradoPorGrupos();
        hacerPeriodo(permutacion);
        lengthmensaje=Double.valueOf(mensajeCifrado.getMensaje().length());
        decifrar(mensajeCifrado, permutacion);
    }
    
    
    
    private void decifrar(Mensaje mensajeCifrado,PermutacionInterfaz permutacion){
        for(contadorPeriodo = 0; contadorPeriodo <= Math.ceil(lengthmensaje/periodoValor); contadorPeriodo++){
            permutarCaracteresPorGrupo(mensajeCifrado, permutacion);
        }
    
    }
     
    
    private  void permutarCaracteresPorGrupo(Mensaje mensajeCifrado,PermutacionInterfaz permutacion){
        for(contadorPermutacion = 1; contadorPermutacion <= this.periodo.getPeriodo(); contadorPermutacion++ ){
            permutaciontemp = permutacion.getPermutacion();
            posicionNuevoCaracter=hacerPosicionNuevaCaracter();
            agregarNuevoCaracterGrupoIncompleto(mensajeCifrado,posicionNuevoCaracter);
            agregarNuevoCaracterGrupoCompleto(mensajeCifrado, posicionNuevoCaracter);
        }
    }
    
    private Integer hacerPosicionNuevaCaracter(){
        Integer posicionNuevoCaracter;
        for(posicionNuevoCaracter=0;permutaciontemp != 0 && (permutaciontemp%10) != contadorPermutacion; permutaciontemp=permutaciontemp/10,posicionNuevoCaracter++){}
        return (contadorPeriodo * periodo.getPeriodo())+(periodo.getPeriodo()-posicionNuevoCaracter)-1;
    }
    
    
    private void agregarNuevoCaracterGrupoCompleto(Mensaje mensajeCifrado,Integer posicionNuevoCaracter){
        if(mensajeDecifrado == null || posicionNuevoCaracter < mensajeCifrado.getMensaje().length()-1 )
            mensajeDecifrado = new MensajeDecifrado(mensajeCifrado.getMensaje().charAt(posicionNuevoCaracter)+"", mensajeDecifrado);
    }
    
    private void agregarNuevoCaracterGrupoIncompleto(Mensaje mensajeCifrado,Integer posicionNuevoCaracter){
        try{
            mensajeCifrado.getMensaje().charAt(posicionNuevoCaracter);
        }
        catch(Exception ex){
            if(((contadorPeriodo * periodo.getPeriodo())+contadorPermutacion) < lengthmensaje )
               mensajeDecifrado = new MensajeDecifrado(mensajeCifrado.getMensaje().charAt((contadorPeriodo * periodo.getPeriodo())+contadorPermutacion)+"", mensajeDecifrado);
        }
    }
    
    
    
    private void hacerPeriodo(PermutacionInterfaz permutacion) throws ExcepcionPeriodo{
        for(contadorPermutacion = permutacion.getPermutacion(); contadorPermutacion != 0; contadorPermutacion = contadorPermutacion/10 , contadorPeriodo++){}
        this.periodo = new Periodo(contadorPeriodo);
        periodoValor=Double.valueOf(periodo.getPeriodo());
    
    }
    
    @Override
    public Mensaje getMensajeDecifrado() {
       return mensajeDecifrado;
    }
    
}
