/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pruebas;

import Cifrado.CifradorPorGrupos;
import Descifrado.DecifradorPorGrupos;
import Excepciones.Cifrado.ExcepcionCifradoPorGrupos;
import Excepciones.Mensaje.ExcepcionMensaje;
import Excepciones.Periodo.ExcepcionPeriodo;
import Excepciones.Permutacion.ExcepcionPermutacion;
import Interfaces.Mensaje.Mensaje;
import Mensaje.Original.MensajeOriginal;
import Mensaje.Cifrado.MensajeCifrado;
import Mensaje.Decifrado.MensajeDecifrado;
import Permutacion.Permutacion;
import Interfaces.Permutacion.PermutacionInterfaz;
import java.util.Arrays;
import Interfaces.Cifrado.Cifrador;
import Interfaces.Decifrador.Decifrador;
import Interfaces.Periodo.PeriodoInterfaz;

/**
 *
 * @author Administrador
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ExcepcionCifradoPorGrupos, ExcepcionPermutacion, ExcepcionPeriodo, ExcepcionMensaje {
        Mensaje mensaje = new MensajeOriginal("TODASLASNACIONESDELAONU");
        System.out.println("Mensaje a cifrar "+mensaje.getMensaje());
        
        PermutacionInterfaz permutacion = new Permutacion(13425);
        Cifrador cifradorPorGrupos = new CifradorPorGrupos(mensaje, permutacion);
        Mensaje mensajeCifrado = cifradorPorGrupos.getMensajeCifrado();
        System.out.println("Mensaje cifrado "+mensajeCifrado.getMensaje());
        
        Decifrador decifrado=new DecifradorPorGrupos(mensajeCifrado,permutacion);
        Mensaje mensajeDecifrado=decifrado.getMensajeDecifrado();
        System.out.println("Mensaje Decifrado"+mensajeDecifrado.getMensaje());
        
        System.out.println();
       
        mensaje = new MensajeOriginal("HOL");
        System.out.println("Mensaje a cifrar "+mensaje.getMensaje());
        cifradorPorGrupos = new CifradorPorGrupos(mensaje, permutacion);
        mensajeCifrado = cifradorPorGrupos.getMensajeCifrado();
        System.out.println("Mensaje cifrado "+mensajeCifrado.getMensaje());
        decifrado=new DecifradorPorGrupos(mensajeCifrado,permutacion);
        mensajeDecifrado=decifrado.getMensajeDecifrado();
        System.out.println("Mensaje Decifrado "+mensajeDecifrado.getMensaje());
        
        
    }
    
    
}
