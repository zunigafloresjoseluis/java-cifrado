/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.Original;
 
import Excepciones.Mensaje.ExcepcionMensaje;
import Interfaces.Mensaje.Mensaje;
public class MensajeOriginal implements Mensaje {
    private String mensaje;
    
    public MensajeOriginal(String mensaje) throws ExcepcionMensaje{
        if(mensaje.isEmpty() || mensaje== null)
            throw new ExcepcionMensaje();
        this.mensaje = mensaje;
    }
    @Override
    public String getMensaje() {
        return mensaje;
    }
    
}
