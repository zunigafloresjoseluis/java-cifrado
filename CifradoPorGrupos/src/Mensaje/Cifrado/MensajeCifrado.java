/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.Cifrado;



import Interfaces.Mensaje.Mensaje;
public class MensajeCifrado implements Mensaje {
    private String mensaje;
    private MensajeCifrado mensajeCifrado=null;
    public MensajeCifrado(String mensaje,MensajeCifrado mensajecifrado){
        if(mensajecifrado!=null){
            this.mensaje = mensajecifrado.getMensaje()+mensaje;
        }
        else{
            this.mensaje=mensaje;
        }
        
    }
    @Override
    public String getMensaje() {
        return mensaje;
    }
    
}
