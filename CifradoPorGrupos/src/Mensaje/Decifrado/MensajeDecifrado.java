/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mensaje.Decifrado;

import Interfaces.Mensaje.Mensaje;
import Mensaje.Cifrado.MensajeCifrado;

 
public class MensajeDecifrado implements Mensaje {
    private String mensaje;
    private MensajeDecifrado mensajeCifrado=null;
    public MensajeDecifrado(String mensaje,MensajeDecifrado mensajecifrado){
        if(mensajecifrado!=null){
            this.mensaje = mensajecifrado.getMensaje()+mensaje;
        }
        else{
            this.mensaje=mensaje;
        }
        
        
    }
    @Override
    public String getMensaje() {
        return mensaje;
    }
    
}
