/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Periodo;

import Excepciones.Periodo.ExcepcionPeriodo;
import Interfaces.Periodo.PeriodoInterfaz;

 
public class Periodo implements PeriodoInterfaz{
    private Integer periodo;
    
    public Periodo(Integer periodo) throws ExcepcionPeriodo{
        if(periodo < 0)
            throw  new ExcepcionPeriodo();
        
        this.periodo = periodo;
    }
   @Override 
    public Integer getPeriodo(){
        return periodo;
    }
}
